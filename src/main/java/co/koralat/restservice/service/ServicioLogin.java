package co.koralat.restservice.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import co.koralat.restservice.entities.Empleado;

@Path("/koralat")
public class ServicioLogin {
	
	@Path("/login")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Empleado validaEmpleado(Empleado empleado) {
		empleado.setRegistrado(false);
		if(empleado.getName().equals("Jon") && empleado.getPassword().equals("1234")) {
			empleado.setRegistrado(true);
		}
		return empleado;
	}
}
